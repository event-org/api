const createError = require('http-errors')
const express = require('express')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const Sequelize = require('sequelize')
const cors = require('cors')
require('dotenv').config()

const userRouter = require('./routes/user')
const eventsRouter = require('./routes/events')
const initializeDB = require('./models')

const sequelize = new Sequelize(process.env.PG_URL);

sequelize
  .authenticate()
  .then(() => {
    sequelize.sync()
    initializeDB(sequelize)
    console.log('Connection has been established successfully.')
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err)
  })


const app = express()

app.use(cors())
app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())


app.use('/user', userRouter)
app.use('/events', eventsRouter)


// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404))
})

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  const statusCode = err.status || 404
  res.status(statusCode).json({
    error: 'Not found',
    status: statusCode
  })
})

module.exports = app
