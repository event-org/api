const Sequelize = require('sequelize')
const sequelize = new Sequelize(process.env.PG_URL)
const Model = Sequelize.Model
class Events extends Model { }

const columns = {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    allowNull: false,
    autoIncrement: true
  },
  eventName: {
    type: Sequelize.STRING,
    allowNull: false
  },
  eventDescription: {
    type: Sequelize.STRING,
    allowNull: false
  },
  maxMember: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  eventDate: {
    type: Sequelize.DATE,
    allowNull: false
  },
  eventTime: {
    type: Sequelize.TIME,
    allowNull: false
  },
  eventLocation: {
    type: Sequelize.STRING,
    allowNull: false
  },
  eventHost: {
    type: Sequelize.STRING,
    allowNull: false
  },
  eventOrganizers: {
    type: Sequelize.ARRAY(Sequelize.STRING),
    allowNull: false
  },
  eventSpeakers: {
    type: Sequelize.ARRAY(Sequelize.STRING),
    allowNull: false
  },
}

const options = {
  sequelize,
  modelName: 'events'
}



const initEvents = sequelize => {
  Events.init(columns, { ...options, sequelize })
}

Events.init(columns, options);



module.exports = { Events, initEvents }
