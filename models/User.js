const Sequelize = require('sequelize')
const sequelize = new Sequelize(process.env.PG_URL)
const Model = Sequelize.Model;
class User extends Model { }

const columns = {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    allowNull: false,
    autoIncrement: true
  },
  username: {
    type: Sequelize.STRING,
    allowNull: false,
    unique: true
  },
  firstName: {
    type: Sequelize.STRING,
    allowNull: false
  },
  middleName: {
    type: Sequelize.STRING
  },
  lastName: {
    type: Sequelize.STRING,
    allowNull: false
  },
  fullName: {
    type: Sequelize.STRING,
    allowNull: false
  },
  email: {
    type: Sequelize.STRING,
    allowNull: false
  },
  password: {
    type: Sequelize.STRING,
    allowNull: false
  }
}

const options = {
  sequelize,
  modelName: 'user'
}

const initUser = sequelize => {
  User.init(columns, { ...options, sequelize })
}

User.init(columns, options);



module.exports = { User, initUser }
