const { initUser } = require('./User')
const { initEvents } = require('./Events')

const models = [initUser, initEvents]


module.exports = function (sequelize) {
  models.forEach(model => {
    model(sequelize)
  });
}