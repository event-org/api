const express = require('express')
const createError = require('http-errors')
const router = express.Router()
// const UserController = require('../models/User')
const { User } = require('../models/User')

/* GET users listing. */
router.get('/:username', function (req, res, next) {
  const { username } = req.params
  if (username) {
    User.findOne({ where: { username } }).then(user => {
      if (user) {
        res.send({ message: 'Successfully retrieved user', data: user })
      }
      else res.send({ ...createError(404), status: 404, data: user })
    });
  }
})

router.post('/add', function (req, res, next) {
  User.create(req.body).then(data => {
    res.json({ data })
  }).catch(({ errors }) => res.status(400).json({ errors: errors.map(error => error.message) }))
})


router.patch('/edit/:username', function (req, res, next) {
  const { username } = req.params
  User.update(req.body, { where: { username } }).then(data => {
    res.json({ data })
  }).catch(err => res.json(err))
})

module.exports = router
