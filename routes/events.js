const express = require('express')
const router = express.Router()
const { Events } = require('../models/Events')

/* GET users listing. */
router.get('/', function (req, res, next) {
  Events.findAll().then(events => {
    res.json({
      message: 'Successfully retrieved users',
      data: events
    })
  })
})


router.post('/add', function (req, res, next) {
  Events.create(req.body).then(data => {
    res.json({ data })
  }).catch(err => res.json(err))
})

router.patch('/edit/:id', function (req, res, next) {
  const { id } = req.params
  Events.update(req.body, { where: { id } }).then(data => {
    res.json({ data })
  }).catch(err => res.json(err))
})

module.exports = router
