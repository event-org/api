# Event Org

- a demo project for Umpisa
- bootstrapped with `express-generator`

# Run Locally

- create an `environment variable`
```bash
PG_URL=postgres://POSTGRES_USER:POSTGRES_PASSWORD@localhost:5432/POSTGRES_DB
```

- install `docker` and `docker-compose`, then execute this in the terminal:
``` bash
docker-compose up
```

- install dependencies
```npm
npm install
```

- start the project, open the browser at `http://localhost:3000`

```npm
npm start
```

# Endpoints

- `/users/:username` [`GET`]
  - request params
    - `username`
  - payload
  ```json
  {
    "message": "Successfully retrieved users",
    "data": {
      "username": "jondoe",
      "firstName": "Jon",
      "middleName": "",
      "lastName": "Doe",
      "fullName": "Jon Doe",
      "email": "jondoe@gmail.com"
    }
  }
  ```

- `/users/add` [`POST`]
  - request body
  ```json
    {
      "username": "jondoe",
      "firstName": "Jon",
      "middleName": "",
      "lastName": "Doe",
      "fullName": "Jon Doe",
      "email": "jondoe@gmail.com",
      "password": "p@ssw0rd"
    }
  ```

- `/users/edit/:username/` [`PATCH`]
  - request params
    - `username`
  - request body
  ```json
    {
      "username": "jondoe",
      "firstName": "Jon",
      "middleName": "",
      "lastName": "Doe",
      "fullName": "Jon Doe",
      "email": "jondoe@gmail.com",
      "password": "p@ssw0rd"
    }
  ```

- `/events`
  - payload
  ```json
    {
      "message": "Successfully retrieved users",
      "data": [
        {
        "eventName": "Sample Event",
        "eventDescription": "Sample Description",
        "maxMember": 10,
        "eventDate": "10/10/2019",
        "eventTime": "9:00 am",
        "eventLocation": "Caloocan",
        "eventHost": "John Doe",
        "eventOrganizers": [
          {
            "username": "org1",
            "mobileNumber": 9991234567
          },
          {
            "username": "org2",
            "mobileNumber": 99891234567
          }
        ],
        "eventSpeakers": [
            {
              "username": "org1",
              "mobileNumber": 9991234567
            },
            {
              "username": "org2",
              "mobileNumber": 9191234567
            }
          ]
        }
      ]
    }
  ```

- `/events/add`
  - request body
  ```json
    {
      "eventName": "Sample Events",
      "eventDescription": "Sample Description",
      "maxMember": 10,
      "eventDate": "10/10/2019",
      "eventTime": "9:00 am",
      "eventLocation": "Caloocan",
      "eventHost": "John Doe",
      "eventSpeakers": ["jhon"],
      "eventOrganizers": ["john"]
    }
  ```

- `/events/edit/:id`
  - request params
    - `id`
  - request body
  ```json
    {
      "eventName": "Sample Events",
      "eventDescription": "Sample Description",
      "maxMember": 10,
      "eventDate": "10/10/2019",
      "eventTime": "9:00 am",
      "eventLocation": "Caloocan",
      "eventHost": "John Doe",
      "eventSpeakers": ["jhon"],
      "eventOrganizers": ["john"]
    }
  ```

- `/unhandled-route`
  - payload
  ```json
    {
        "error": "Not found",
        "status": 404
    }
  ```
